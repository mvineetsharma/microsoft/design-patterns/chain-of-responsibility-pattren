﻿using System;

// The Fruit class represents the object that will be processed by the chain.
class Fruit
{
    public string Name { get; set; }
    public int Size { get; set; }

    public Fruit(string name, int size)
    {
        Name = name;
        Size = size;
    }
}

// The Handler interface defines the method for handling requests.
interface IFruitHandler
{
    void Handle(Fruit fruit);
}

// ConcreteHandler classes implement the handling logic.
class SmallFruitHandler : IFruitHandler
{
    private IFruitHandler _nextHandler;

    public void SetNextHandler(IFruitHandler handler)
    {
        _nextHandler = handler;
    }

    public void Handle(Fruit fruit)
    {
        if (fruit.Size < 5)
        {
            Console.WriteLine($"{fruit.Name} is small. Handled by SmallFruitHandler.");
        }
        else if (_nextHandler != null)
        {
            _nextHandler.Handle(fruit);
        }
    }
}

class MediumFruitHandler : IFruitHandler
{
    private IFruitHandler _nextHandler;

    public void SetNextHandler(IFruitHandler handler)
    {
        _nextHandler = handler;
    }

    public void Handle(Fruit fruit)
    {
        if (fruit.Size >= 5 && fruit.Size < 10)
        {
            Console.WriteLine($"{fruit.Name} is medium. Handled by MediumFruitHandler.");
        }
        else if (_nextHandler != null)
        {
            _nextHandler.Handle(fruit);
        }
    }
}

class LargeFruitHandler : IFruitHandler
{
    public void Handle(Fruit fruit)
    {
        if (fruit.Size >= 10)
        {
            Console.WriteLine($"{fruit.Name} is large. Handled by LargeFruitHandler.");
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        var smallHandler = new SmallFruitHandler();
        var mediumHandler = new MediumFruitHandler();
        var largeHandler = new LargeFruitHandler();

        // Create the chain of responsibility.
        smallHandler.SetNextHandler(mediumHandler);
        mediumHandler.SetNextHandler(largeHandler);

        // Process fruits.
        var apple = new Fruit("Apple", 7);
        var grape = new Fruit("Grape", 3);
        var watermelon = new Fruit("Watermelon", 12);

        smallHandler.Handle(apple);
        smallHandler.Handle(grape);
        smallHandler.Handle(watermelon);
    }
}
